# **README AUTOMATIZACION MOBILE**
Proyecto de automatizacion mobiles con screenplay y appium de app desarrollada con flutter

***

Requisitos del proyecto:
* [Gradle]: version 7.4 
* [JDk]: version 11
* [Serenity]: version 3.3.1
* [Appium_java_client]: version 8.3.0
* [Devco automation]: version 2.0.2

## **Run tests Chrome gradle:**
```
./gradle clean test 
./gradle test --tests "****"
```

